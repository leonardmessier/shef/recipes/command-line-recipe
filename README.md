# Command line tools

This installs various handy command line tools like :

- [fzf](https://github.com/junegunn/fzf)
- [direnv](https://direnv.net/)
- [t](https://stevelosh.com/projects/t)
